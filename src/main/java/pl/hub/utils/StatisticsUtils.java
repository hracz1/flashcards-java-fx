package pl.hub.utils;

import pl.hub.lang.practices.Word;
import pl.hub.lang.statistics.Result;
import pl.hub.lang.statistics.Statistics;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class StatisticsUtils {

    private static final Map<String, Integer> GRADES_MAP;

    static {
        GRADES_MAP = new HashMap<>();
        GRADES_MAP.put("A", 4);
        GRADES_MAP.put("B", 3);
        GRADES_MAP.put("C", 2);
        GRADES_MAP.put("F", 1);
    }

    public static Word getMostOften(List<Statistics> statisticsList) {
        List<Word> wordList = new ArrayList<>();
        statisticsList.forEach(s -> wordList.addAll(s.getListOfWords()));
        return countMax(wordList);
    }

    private static Word countMax(List<Word> wordList) {
        if (wordList == null || wordList.isEmpty()) {
            return null;
        }
        wordList.sort((o1, o2) -> Double.compare(o2.submit(), o1.submit()));
        return wordList.get(0);
    }

    public static Word getLeastOftern(List<Statistics> statisticsList) {
        List<Word> wordList = new ArrayList<>();
        statisticsList.forEach(s -> wordList.addAll(s.getListOfWords()));
        return countMin(wordList);
    }

    private static Word countMin(List<Word> wordList) {
        if (wordList == null || wordList.isEmpty()) {
            return null;
        }
        wordList.sort(Comparator.comparingDouble(Word::submit));
        return wordList.get(0);
    }

    public static double getValidity(List<Statistics> stats) {
        double[] good = {0};
        double[] all = {0};

        stats.forEach(e -> {
            good[0] += e.getCounterGood();
            all[0] += e.getAllCounter();
        });
        return 100 * good[0] / all[0];
    }

    public static List<Word> sumStatsAndReturnAsList(List<Statistics> statisticsList){
        List<Word> sumWordList = new ArrayList<>();
        for (Statistics statistics : statisticsList) {
            List<Word> listOfWords = statistics.getListOfWords();
            for (Word word : listOfWords) {
                if (sumWordList.contains(word)) {
                    sumWordList.stream()
                            .filter(s -> s.getPol().equals(word.getPol()))
                            .findFirst()
                            .ifPresent(s -> {
                                s.setGood(s.getGood() + word.getGood());
                                s.setBad(s.getBad() + word.getBad());
                            });
                } else {
                    if (word.getGood() == 0 && word.getBad() == 0) {
                        continue;
                    }
                    sumWordList.add(word);
                }
            }
        }
        return sumWordList;
    }

    public static void saveStatisticsToFile(File file, List<Statistics> statisticsList) {
        if (statisticsList.stream().anyMatch(Objects::isNull)) {
            return;
        }
        List<Word> wordList = sumStatsAndReturnAsList(statisticsList);
        List<String> resultLines = new ArrayList<>();
        resultLines.add(String.format("Ogólny wynik poprawności podczas praktyki to: %.2f", getValidity(statisticsList)));
        for (Word word : wordList) {
            if (word.getBad() == 0 && word.getGood() == 0) {
                continue;
            }
            resultLines.add(String.format("Słowo %s zostało podane porpawnie w sumie %.2f ", word.getPol(), word.submit()) + " % razy");
        }

        try {
            Files.write(Paths.get(file.getAbsolutePath()), resultLines);
        } catch (IOException e) {
            e.printStackTrace();
            CourseInfo.showError("Błąd zapisu statystyk!");
        }
    }

    public static String getBestGrade(List<Result> results) {
        int max = 1;
        String grade = "F";
        for (Result result : results) {
            if (GRADES_MAP.get(result.getGrade()) > max) {
                max = GRADES_MAP.get(result.getGrade());
                grade = result.getGrade();
            }
        }
        return grade;
    }

    public static String getWorstGrade(List<Result> results) {
        int min = 4;
        String grade = "A";
        for (Result result : results) {
            if (GRADES_MAP.get(result.getGrade()) < min) {
                min = GRADES_MAP.get(result.getGrade());
                grade = result.getGrade();
            }
        }
        return grade;
    }

}
