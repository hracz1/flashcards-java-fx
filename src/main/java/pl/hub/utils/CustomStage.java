package pl.hub.utils;

import javafx.stage.Stage;
import pl.hub.controllers.AboveController;

public class CustomStage extends Stage {
    public DataTransferDTO showAndReturn(AboveController controller){
        super.showAndWait();
        return controller.returnData();
    }

}
