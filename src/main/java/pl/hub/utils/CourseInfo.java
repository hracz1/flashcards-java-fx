package pl.hub.utils;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class CourseInfo {
    private static final StackPane pane;
    private static final Label label;
    private static final Stage stage;
    private static final Scene scene;


    static {
        pane = new StackPane();
        pane.setPrefHeight(100);
        pane.setPrefWidth(200);
        label = new Label("");
        label.setFont(new Font(20));
        pane.getChildren().add(label);
        scene = new Scene(pane, 400, 100);
        stage = new Stage();
        stage.setScene(scene);
    }


    public static void showError(String message) {
        clearAndConf(message);
        stage.setTitle("Błąd!");
        stage.show();
    }

    public static void showInfo(String message) {
        clearAndConf(message);
        stage.setTitle("Informacja!");
        stage.show();
    }

    private static void clearAndConf(String message) {
        pane.getChildren().clear();
        label.setText(message);
        pane.getChildren().add(label);
        scene.setRoot(pane);
        stage.setScene(scene);
    }

    public void showAsInstance(String message){
        StackPane pane = new StackPane();
        pane.setPrefHeight(100);
        pane.setPrefWidth(200);
        Label label = new Label(message);
        label.setFont(new Font(20));
        pane.getChildren().add(label);
        Scene scene = new Scene(pane, 400, 100);
        Stage stage = new Stage();
        stage.setTitle("Wiadomość od admina!");
        stage.setScene(scene);
        stage.show();
    }
}
