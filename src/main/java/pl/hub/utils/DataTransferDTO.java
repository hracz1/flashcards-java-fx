package pl.hub.utils;

import java.util.HashMap;
import java.util.Map;

public class DataTransferDTO {
    private final Map<String, Object> map = new HashMap<>();

    public boolean add(String name, Object object) {
        if (name != null && object != null) {
            map.put(name, object);
            return true;
        }
        return false;
    }

    public Object get(String name){
        return map.get(name);
    }

    public boolean containsKey(String name){
        return map.containsKey(name);
    }

    public Map<String, Object> getParams() {
        return map;
    }

    public static DataTransferDTO joinIntoOneDTO(DataTransferDTO... dataTransferDTOS){
        DataTransferDTO dto = new DataTransferDTO();
        for (DataTransferDTO dataTransferDTO : dataTransferDTOS) {
            Map<String, Object> params = dataTransferDTO.getParams();
            params.forEach(dto::add);
        }
        return dto;
    }
}
