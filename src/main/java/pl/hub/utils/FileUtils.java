package pl.hub.utils;

import pl.hub.security.PasswordManager;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.AccessControlContext;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class FileUtils {
    private static final String regex = "[a-zA-Z]+-[a-zA-Z]+";

    public static boolean isGoodFormat(File file) throws IOException {

        if (file == null || !file.getName().endsWith(".txt")) {
            return false;
        }
        return Files.lines(Paths.get(file.getAbsolutePath()), Charset.forName("windows-1250"))
                .allMatch(s -> s.matches(regex));

    }

    public static Map<String, String> getFlashesAsMap(File flashcardsFile) {
        try {
            Function<String, String> keyCreator = s1 -> s1.split("-")[0].trim();
            Function<String, String> valueCreator = s1 -> s1.split("-")[1].trim();


            return Files.lines(Paths.get(flashcardsFile.getAbsolutePath()))
                    .collect(Collectors.toMap(keyCreator, valueCreator, (e1, e2) -> e1, LinkedHashMap::new));

        } catch (IOException e) {
            return null;
        }

    }

    public static void saveToFile(File file, Map<String, String> map) {
        List<String> list = new ArrayList<>();
        map.forEach((k, v) -> list.add(k + "-" + v));
        try {
            Files.write(Paths.get(file.getAbsolutePath()), list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveToTempFile(File file, List<Map<String, String>> listMap) {
        try {
            Files.write(Paths.get(file.getAbsolutePath()), convert(listMap));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> convert(List<Map<String, String>> list) {
        return list.stream()
                .map(s -> s.get("pol") + "-" + s.get("ang"))
                .collect(Collectors.toCollection(ArrayList::new));
    }


    public static void saveToFile(File file, String text) {
        try {
            Files.write(Paths.get(file.getAbsolutePath()), Collections.singletonList(text), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveToFile(File file, List<String> list) {
        try {
            Files.write(Paths.get(file.getAbsolutePath()), list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
