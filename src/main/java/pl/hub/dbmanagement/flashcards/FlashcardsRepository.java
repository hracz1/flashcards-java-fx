package pl.hub.dbmanagement.flashcards;

import pl.hub.model.User;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

public interface FlashcardsRepository {
    Connection getConnection();
    List<Map<String, String>> getFlashes(User user);
    void updateOrInsertFlashcards(Map<String, String> map, User user);
    long getNumberOfFlashes(User user);
}
