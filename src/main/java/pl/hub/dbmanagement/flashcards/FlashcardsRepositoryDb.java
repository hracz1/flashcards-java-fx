package pl.hub.dbmanagement.flashcards;

import pl.hub.dbmanagement.connection.AbstractRepo;
import pl.hub.model.User;
import pl.hub.utils.CourseInfo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

public class FlashcardsRepositoryDb extends AbstractRepo implements FlashcardsRepository {

    @Override
    public List<Map<String, String>> getFlashes(User user) {
        String query = "" +
                "select " +
                "  a.pol,a.ang " +
                "from " +
                "  Flashes a " +
                "where " +
                "  a.usersId = " + user.getId();

        List<Map<String, Object>> resultAsList = getResultAsList(query);
        if (resultAsList == null || resultAsList.isEmpty() || resultAsList.stream().anyMatch(Objects::isNull)) {
            return null;
        }
        return resultAsList.stream()
                .map(s -> {
                    Map<String, String> map = new HashMap<>();
                    s.forEach((k, v) -> map.put(k, (String) v));
                    return map;
                })
                .collect(Collectors.toList());

    }

    @Override
    public void updateOrInsertFlashcards(Map<String, String> map, User user) {

        List<ResultSet> resultSets = new ArrayList<>();
        ResultSet resultSet;
        try {
            Set<String> linked = new LinkedHashSet<>(map.keySet());
            for (String key : linked) {
                String s = "" +
                        "select if( exists(    " +
                        "     select           " +
                        "            -1        " +
                        "     from             " +
                        "            Flashes " +
                        "     where            " +
                        "            pol = '" + key + "' and usersId = " + user.getId() + " ),1,0)";
                resultSet = getPreparedStatemnt(s).executeQuery();
                resultSets.add(resultSet);
            }
            Iterator<String> stringIterator = linked.iterator();

            for (ResultSet rs : resultSets) {
                while (rs.next() && stringIterator.hasNext()) {
                    String nextKey = stringIterator.next();
                    long res = (Long) rs.getObject(1);
                    String queryEl;
                    if (res == 1) {
                        if (rs.getObject(2).equals(map.get(nextKey))) {
                            continue;
                        }
                        queryEl = "" +
                                "update " +
                                "  Flashes a " +
                                "set " +
                                "  ang = '" + map.get(nextKey) + "' " +
                                "where " +
                                "  pol = '" + nextKey + "' " +
                                "and " +
                                "  usersId = " + user.getId();
                    } else {
                        System.out.println("Im inserting");
                        queryEl = "" +
                                "insert " +
                                "into Flashes " +
                                "  (pol,ang,usersId) " +
                                "values " +
                                "  ('" + nextKey + "','" + map.get(nextKey) + "'," + user.getId() + ")";
                    }
                    getPreparedStatemnt(queryEl).executeUpdate();
                }
            }
            CourseInfo.showInfo("Pomyslny zapis!");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            CourseInfo.showError("Blad w zapisie rekordów!");
        }
    }

    @Override
    public long getNumberOfFlashes(User user) {
        String query = "" +
                "select " +
                "  count(a.id) as number " +
                "from " +
                "  Flashes a " +
                "where " +
                "  a.usersId = " + user.getId();

        List<Map<String, Object>> resultAsList = getResultAsList(query);

        if (resultAsList == null || resultAsList.isEmpty() || resultAsList.stream().anyMatch(Objects::isNull)) {
            return 0;
        }

        return (Long) resultAsList.get(0).get("number");
    }

}
