package pl.hub.dbmanagement.admin;

import pl.hub.dbmanagement.connection.AbstractRepo;
import pl.hub.utils.CourseInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminRepositoryDb extends AbstractRepo implements AdminRepository {
    @Override
    public Map<Integer, String> getMapOfUsersNamesWithId() {
        String query = "" +
                "select " +
                "  idUsers,login " +
                "from " +
                "  users ";

        List<Map<String, Object>> rs = getResultAsList(query);
        if (rs == null || rs.isEmpty()) {
            return null;
        }

        Map<Integer, String> map = new HashMap<>();
        for (Map<String, Object> r : rs) {
            map.put((Integer) r.get("idUsers"), (String) r.get("login"));
        }
        return map;
    }


    @Override
    public Map<String, Integer> getExamResultsByUserId(int userId) {
        String query = "" +
                "select " +
                "  s.positive_exam_attempts, s.exam_attempts " +
                "from " +
                "  statistics s " +
                "where " +
                "  s.idUser = " + userId;

        return getListByQuery(query);
    }

    private Map<String, Integer> getListByQuery(String query) {
        List<Map<String, Object>> resultAsList = getResultAsList(query);
        Map<String, Integer> outputMap = new HashMap<>();
        if (resultAsList == null || resultAsList.isEmpty()) {

            CourseInfo.showError("Blad w zapytaniu " + query);
            return null;
        }
        for (Map.Entry<String, Object> stringObjectEntry : resultAsList.get(0).entrySet()) {
            outputMap.put(stringObjectEntry.getKey(), (Integer) stringObjectEntry.getValue());
        }
        System.out.println(outputMap);
        return outputMap;
    }

    @Override
    public Map<String, Integer> getValidityByUserId(int userId) {

        String query = "" +
                "select " +
                "  s.good, s.all_fc " +
                "from " + "  statistics s " +
                "where " +
                "  s.idUser = " + userId;

        return getListByQuery(query);
    }

    @Override
    public boolean getAccountStatusByUserId(int userId) {
        String query = "" +
                "select " +
                "  status " +
                "from " +
                "  users " +
                "where " +
                "  idUsers = " + userId;
        List<Map<String, Object>> data = getResultAsList(query);
        if (data == null || data.isEmpty()) {
            return false;
        }
        return "T".equals(data.get(0).get("status"));
    }

    @Override
    public void lockAccount(int userId) {
        String query = "" +
                "update " +
                "  users " +
                "set  " +
                "  status = 'N' " +
                "where " +
                "  idUsers = " + userId;

        updateData(query);
    }

    @Override
    public void unlockAccount(int userId) {
        String query = "" +
                "update " +
                "  users " +
                "set  " +
                "  status = 'T' " +
                "where " +
                "  idUsers = " + userId;

        updateData(query);
    }

    @Override
    public void setMessageForUser(int idByLogin, String text) {

    }
}
