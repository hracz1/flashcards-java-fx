package pl.hub.dbmanagement.admin;

import java.util.Map;

public interface AdminRepository {
    Map<Integer, String> getMapOfUsersNamesWithId();
    Map<String, Integer> getExamResultsByUserId(int userId);
    Map<String, Integer> getValidityByUserId(int userId);
    boolean getAccountStatusByUserId(int userId);
    void lockAccount(int userId);
    void unlockAccount(int userId);
    void setMessageForUser(int idByLogin, String text);
}
