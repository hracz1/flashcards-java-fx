package pl.hub.dbmanagement.usersDB;

import pl.hub.lang.statistics.Result;
import pl.hub.lang.statistics.Statistics;

public interface UserStatsRepository {
    void saveStatistics(Statistics statistics, int userId);

    void saveExamStatistics(Result result, int userId);

    void insertStatsRecord(int userId);
}
