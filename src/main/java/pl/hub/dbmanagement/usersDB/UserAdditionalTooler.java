package pl.hub.dbmanagement.usersDB;

import pl.hub.dbmanagement.messages.MessageRepository;
import pl.hub.dbmanagement.messages.MessageRepositoryDb;
import pl.hub.model.User;

public class UserAdditionalTooler {
    private final UserRepository userRepository;
    private final MessageRepository messageRepository;
    private final User user;

    public UserAdditionalTooler(User user) {
        this.user = user;
        userRepository = new UserRepositoryDb();
        messageRepository = new MessageRepositoryDb();
    }

    public User fillId() {
        int idByLogin = userRepository.getIdByLogin(user.getLogin());
        user.setId(idByLogin);
        return user;
    }

    public boolean isActive(){
       return userRepository.getAccountStatus(user.getId());
    }

    public User fillMessages(){
        user.setMessages(messageRepository.getAndDeleteMessagesForUser(user.getId()));
        return user;
    }
}
