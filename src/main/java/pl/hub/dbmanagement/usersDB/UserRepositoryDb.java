package pl.hub.dbmanagement.usersDB;

import pl.hub.dbmanagement.connection.AbstractRepo;
import pl.hub.lang.statistics.Statistics;
import pl.hub.model.User;
import pl.hub.security.Role;
import pl.hub.utils.CourseInfo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class UserRepositoryDb extends AbstractRepo implements UserRepository {

    public UserRepositoryDb() {
        super();
    }

    @Override
    public ResultSet getUserStatsByLogin(String login) {
        String query = "" +
                "select " +
                "  b.results  " +
                "from " +
                "  Users a " +
                "where " +
                "  a.login = '" + login + "'";
        return getData(query);
    }

    @Override
    public boolean saveUser(User user) {
        String query = "" +
                "insert into " +
                "  Users (login,password,role) " +
                "values " +
                "  (?,?,?)";

        PreparedStatement preparedStatement = getPreparedStatemnt(query);
        try {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getRole().toString());
            preparedStatement.execute();
            CourseInfo.showInfo("Uzytkownik " + user.getLogin() + " został zarejestrowany!");
            return true;
        } catch (SQLException throwables) {
            CourseInfo.showError("Uzytkownik " + user.getLogin() + " istnieje. " + System.lineSeparator() + "Wybierz inny login!");
            return false;
        }
    }

    @Override
    public Integer getIdByLogin(String login) {
        String query = "" +
                "select " +
                "  a.idUsers " +
                "from " +
                "  Users a " +
                "where " +
                "  a.login = '" + login + "'";
        List<Map<String, Object>> resultAsList = getResultAsList(query);
        if (resultAsList != null && !resultAsList.isEmpty() && resultAsList.get(0).get("idUsers") != null) {
            return (Integer) resultAsList.get(0).get("idUsers");
        }
        return -1;
    }

    public boolean getAccountStatus(int userId){
        String query = "" +
                "select " +
                "  status " +
                "from " +
                "  users " +
                "where " +
                "  idUsers = " + userId;
        List<Map<String, Object>> data = getResultAsList(query);
        if (data == null || data.isEmpty()) {
            return false;
        }
        return "T".equals(data.get(0).get("status"));
    }

    @Override
    public Role getUserRole(String login) {
        return null;
    }

    @Override
    public String getEncodedPassword(String pass) {
        return null;
    }


}
