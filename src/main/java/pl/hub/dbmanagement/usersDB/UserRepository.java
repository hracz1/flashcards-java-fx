package pl.hub.dbmanagement.usersDB;

import pl.hub.lang.statistics.Statistics;
import pl.hub.model.User;
import pl.hub.security.Role;

import java.sql.ResultSet;

public interface UserRepository {
    ResultSet getUserStatsByLogin(String login);
    boolean saveUser(User user);
    Role getUserRole(String login);
    String getEncodedPassword(String login);
    Integer getIdByLogin(String login);
    boolean getAccountStatus(int id);
}
