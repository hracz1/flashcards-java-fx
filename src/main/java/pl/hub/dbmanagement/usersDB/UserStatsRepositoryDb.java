package pl.hub.dbmanagement.usersDB;

import pl.hub.dbmanagement.connection.AbstractRepo;
import pl.hub.lang.statistics.Result;
import pl.hub.lang.statistics.Statistics;
import pl.hub.utils.CourseInfo;

import java.util.List;
import java.util.Map;

public class UserStatsRepositoryDb extends AbstractRepo implements UserStatsRepository {


    @Override
    public void saveStatistics(Statistics statistics, int userId) {

        String query = "" +
                "select " +
                "  s.good, s.all_fc " +
                "from " +
                "  statistics s " +
                "where " +
                "  idUser = " + userId;

        List<Map<String, Object>> resultAsList = getResultAsList(query);
        if (resultAsList == null || resultAsList.isEmpty()) {
            CourseInfo.showError("Blad w zapytaniu " + query);
            return;
        }
        int oldGood = (int) resultAsList.get(0).get("good");
        int oldAll = (int) resultAsList.get(0).get("all_fc");

        String updateQuery = "" +
                "update " +
                "  statistics s " +
                "set " +
                "  s.good = " + (oldGood + statistics.getCounterGood()) + " , s.all_fc = " + (oldAll + statistics.getAllCounter()) + " " +
                "where " +
                "  idUser = " + userId;

        updateData(updateQuery);

    }

    @Override
    public void saveExamStatistics(Result result, int userId) {

        String query = "" +
                "select " +
                "  s.exam_attempts, " +
                "  s.positive_exam_attempts " +
                "from " +
                "  statistics s " +
                "where " +
                "  idUser = " + userId;

        List<Map<String, Object>> resultAsList = getResultAsList(query);
        if (resultAsList == null || resultAsList.isEmpty()) {
            CourseInfo.showError("Blad w zapytaniu " + query);
            return;
        }
        int oldGood = (int) resultAsList.get(0).get("positive_exam_attempts");
        int oldAll = (int) resultAsList.get(0).get("exam_attempts");

        String updateQuery = "" +
                "update " +
                "  statistics s " +
                "set " +
                "  s.positive_exam_attempts = " + (!"F".equals(result.getGrade()) ? (oldGood + 1) : oldGood) + ", " +
                "  s.exam_attempts = " + (oldAll + 1) + " " +
                "where " +
                "  idUser = " + userId;

        updateData(updateQuery);

    }

    @Override
    public void insertStatsRecord(int userId) {
        String query = "" +
                "insert into " +
                "  statistics " +
                "(good,all_fc,exam_attempts,positive_exam_attempts,idUser) " +
                "  values " +
                "(0,0,0,0," + userId + ")";
        updateData(query);
    }


}
