package pl.hub.dbmanagement.usersDB;

import pl.hub.security.Role;

import java.util.List;
import java.util.Map;

public class UserRepositorySecurityDb extends UserRepositoryDb {
    public UserRepositorySecurityDb() {
        super();
    }

    @Override
    public Role getUserRole(String login) {
        String query = "" +
                "select " +
                "  a.role " +
                "from " +
                "  Users a " +
                "where " +
                "  a.login = '" + login + "' " +
                "limit 1";
        List<Map<String, Object>> rs = getResultAsList(query);
        if (rs != null && !rs.isEmpty() && rs.get(0) != null && !rs.get(0).isEmpty()) {
            return Role.valueOf((String) rs.get(0).get("role"));
        }
        return Role.COMMON;
    }

    @Override
    public String getEncodedPassword(String login) {
        String query = "" +
                "select " +
                "  a.password " +
                "from " +
                "  Users a " +
                "where " +
                "  a.login = '" + login + "' " +
                "limit 1";
        List<Map<String, Object>> resultAsList = getResultAsList(query);
        return !resultAsList.isEmpty() ? (String) resultAsList.get(0).get("password") : null;
    }


}
