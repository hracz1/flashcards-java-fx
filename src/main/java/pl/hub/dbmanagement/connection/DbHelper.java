package pl.hub.dbmanagement.connection;

import pl.hub.utils.CourseInfo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class DbHelper {
    private static final String HOST = "jdbc:mysql://localhost:3306/fiszki?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection(HOST, "root", "admin");
        } catch (SQLException e) {
            e.printStackTrace();
            CourseInfo.showError("Błąd połączenia z bazą danych!");
        }
        return null;
    }

    public void closeConnection() {
// dobre pytanie jak to zrobic....
    }
}
