package pl.hub.dbmanagement.connection;

import pl.hub.utils.CourseInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepo {

    protected final Connection connection;

    public AbstractRepo() {
        connection = DbHelper.getConnection();
    }

    private ResultSet getResultSetByQuery(String query) {
        try {
            return connection.prepareStatement(query).executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            CourseInfo.showError("Bład w zapytaniu: " + query);
        }
        return null;
    }

    public ResultSet getData(final String query) {
        return getResultSetByQuery(query);
    }

    public List<Map<String, Object>> getResultAsList(final String query) {
        ResultSet resultSet = getResultSetByQuery(query);
        List<Map<String, Object>> list = new ArrayList<>();
        if (resultSet == null) {
            return null;
        }

        try {
            while (resultSet.next()) {
                Map<String, Object> map = new LinkedHashMap<>();
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    map.put(resultSet.getMetaData().getColumnName(i), resultSet.getObject(i));
                }
                list.add(map);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            CourseInfo.showError("Bład w zapytaniu: " + query);
        }
        return null;
    }

    public void updateData(String query) {
        try {
            connection.prepareStatement(query).executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            CourseInfo.showError("Bład w zapytaniu: " + query);
        }
    }

    public PreparedStatement getPreparedStatemnt(String sql) {
        try {
            return connection.prepareStatement(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public Connection getConnection() {
        return connection;
    }


    public int executeQueries(List<String> listOfQueries) {
        try {
            try {
                connection.setAutoCommit(false);
                listOfQueries.forEach(this::updateData);
                connection.commit();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                connection.rollback();
                throwables.printStackTrace();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            CourseInfo.showError("Blad w wykonywaniu transakcji!");
        }
        return 1;
    }

}
