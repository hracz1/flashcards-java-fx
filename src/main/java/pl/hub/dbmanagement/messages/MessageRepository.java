package pl.hub.dbmanagement.messages;

import java.util.List;

public interface MessageRepository {
    void saveMessageForUser(String text,int userId);
    String getNewestMessageForUser(int userId);
    List<String> getAndDeleteMessagesForUser(int userId);
}
