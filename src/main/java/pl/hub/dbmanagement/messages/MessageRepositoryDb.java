package pl.hub.dbmanagement.messages;

import pl.hub.dbmanagement.connection.AbstractRepo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MessageRepositoryDb extends AbstractRepo implements MessageRepository {
    @Override
    public void saveMessageForUser(String text, int userId) {
        PreparedStatement preparedStatement = getPreparedStatemnt("" +
                "insert into " +
                "  messages " +
                "(text,userId) " +
                "  values " +
                "(?,?)");

        try {
            preparedStatement.setString(1, text);
            preparedStatement.setInt(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getNewestMessageForUser(int userId) {
        String query = "" +
                "select " +
                "  text " +
                "from " +
                "  messages " +
                "where " +
                "  id = ( " +
                "       select " +
                "           min(id) " +
                "       from " +
                "           messages " +
                "       where " +
                "           userId = " + userId + ")";

        List<Map<String, Object>> resultAsList = getResultAsList(query);
        if (resultAsList == null || resultAsList.isEmpty() || resultAsList.stream().anyMatch(Objects::isNull)) {
            return null;
        }
        return (String) resultAsList.get(0).get("text");

    }

    @Override
    public List<String> getAndDeleteMessagesForUser(int userId) {
        String messagesForUser = "" +
                "select " +
                "  text " +
                "from " +
                "  messages " +
                "where " +
                "  userId = " + userId;

        ResultSet data = getData(messagesForUser);

        List<String> messages = new ArrayList<>();
        if (data == null) {
            return null;
        }

        try {
            while (data.next()) {
                String text = data.getObject(1, String.class);
                messages.add(text);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        String query = "" +
                "delete from " +
                "  messages " +
                "where " +
                "  userId = " + userId;

        updateData(query);
        return messages;
    }
}
