package pl.hub.controllers;

import pl.hub.utils.DataTransferDTO;

public interface AboveController {
    DataTransferDTO returnData();
    void setInitialData(DataTransferDTO dataDTO);
}
