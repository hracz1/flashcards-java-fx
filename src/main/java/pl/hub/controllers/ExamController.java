package pl.hub.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.hub.lang.exam.ExamMaker;
import pl.hub.lang.statistics.Result;
import pl.hub.lang.timer.TimerExecutor;
import pl.hub.model.User;
import pl.hub.utils.CourseInfo;
import pl.hub.utils.DataTransferDTO;
import pl.hub.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExamController implements AboveController, Initializable {

    @FXML
    private Label presentWordLabel;

    @FXML
    private Label remainingTimeLabel;

    @FXML
    private Label remainingQuesLabel;

    @FXML
    private Button startBtn;

    @FXML
    private Button printBtn;

    @FXML
    private TextField userAnswer;

    @FXML
    private Button resultBtn;

    @FXML
    private Label insivisbleLabelFlag;

    private File file;
    private ExecutorService executorService;
    private ExamMaker examMaker;
    private Result result;
    private TimerExecutor timer;
    private User user;

    @Override
    public DataTransferDTO returnData() {
        DataTransferDTO dataTransferDTO = new DataTransferDTO();
        dataTransferDTO.add("results", Objects.requireNonNullElseGet(result, () -> new Result("0", null, "F")));
        return dataTransferDTO;
    }

    @Override
    public void setInitialData(DataTransferDTO dataDTO) {
        if (dataDTO != null && dataDTO.containsKey("plik")) {
            file = new File((String) dataDTO.get("plik"));
        }
        if (dataDTO != null && dataDTO.containsKey("user")) {
            user = (User) dataDTO.get("user");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        insivisbleLabelFlag.setVisible(false);
        resultBtn.setDisable(true);

        remainingTimeLabel.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("Czas minął")) {
                if (!timer.isCancelled()) {
                    CourseInfo.showError("Czas minął!");
                    ((Stage) presentWordLabel.getScene().getWindow()).close();
                }
                resultBtn.setDisable(false);
            }
        });
        insivisbleLabelFlag.textProperty().addListener((observable, oldValue, newValue) -> stopTimer());
        printBtn.setVisible(false);
    }

    private void stopTimer() {
        timer.cancel();
        executorService.shutdownNow();
    }

    @FXML
    void start() {
        startBtn.setDisable(true);
        DataTransferDTO dto = new DataTransferDTO();
        dto.add("file", file);
        dto.add("userAnswer", userAnswer);
        dto.add("presentWordLabel", presentWordLabel);
        dto.add("remainingQuestions", remainingQuesLabel);
        dto.add("flag", insivisbleLabelFlag);
        dto.add("user", user);

        configureTimer();

        examMaker = new ExamMaker(dto);
        examMaker.executeExam();
    }


    @FXML
    void results() throws IOException {
        result = examMaker.saveAndGetResultObject();
        CourseInfo.showInfo("Pobrano wyniki egzaminu! Ocena: " + result.getGrade() + "\n Więcej informacji w zakładce Osiągi");
        if (Arrays.asList("A", "B", "C").contains(result.getGrade())) {
            printBtn.setVisible(true);
        }
    }

    @FXML
    void printCert() {
        String certificationPattern = result.getCertificationPattern();
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showSaveDialog(printBtn.getScene().getWindow());
        if (file == null) {
            return;
        }
        try {
            Files.write(Paths.get(file.getAbsolutePath()), Collections.singletonList(certificationPattern));
            CourseInfo.showInfo("Certyfikat został zapisany pod nazwą " + file.getName());
        } catch (IOException e) {
            e.printStackTrace();
            CourseInfo.showError("Błąd wydruku danych!");
        }

    }

    private void configureTimer() {
        timer = new TimerExecutor(Objects.requireNonNull(FileUtils.getFlashesAsMap(file)).size() * 3);
        remainingTimeLabel.textProperty().bind(timer.messageProperty());
        executorService = Executors.newFixedThreadPool(1);
        executorService.execute(timer);
        executorService.shutdown();
    }

}

