

package pl.hub.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import pl.hub.lang.practices.Word;
import pl.hub.lang.statistics.Result;
import pl.hub.lang.statistics.Statistics;
import pl.hub.utils.CourseInfo;
import pl.hub.utils.DataTransferDTO;
import pl.hub.utils.StatisticsUtils;

import java.io.File;
import java.net.URL;
import java.util.*;

public class StatisticsController implements AboveController, Initializable {

    @FXML
    private BorderPane mainNode;

    @FXML
    private BarChart<String, Number> barChart;

    @FXML
    private CategoryAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private Label wholeValiditylabel;

    @FXML
    private Button printStatsBtn;

    @FXML
    private Label examAttemptsLabel;

    @FXML
    private Label hardestWord;

    @FXML
    private Label easiestWord;

    @FXML
    private Label bestGradeLabel;

    @FXML
    private Label worstGradeLabel;

    private List<Statistics> statistics;
    private Word maxWord;
    private Word minWord;
    private List<Result> results;


    @FXML
    void printStats(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showSaveDialog(((Node) event.getSource()).getScene().getWindow());
        StatisticsUtils.saveStatisticsToFile(file, statistics);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    private void configureChart() {
        ObservableList<XYChart.Data<String, Number>> data = FXCollections.observableArrayList();
        List<Word> sumWordList = StatisticsUtils.sumStatsAndReturnAsList(statistics);
        sumWordList.forEach(s -> data.add(new XYChart.Data<>(s.getPol(), s.submit())));

        XYChart.Series<String, Number> series = new XYChart.Series<>("serie", data);
        series.setData(data);
        barChart.getData().addAll(series);
    }

    @Override
    public DataTransferDTO returnData() {
        return null;
    }

    @Override
    public void setInitialData(DataTransferDTO dataDTO) {
        if (dataDTO.containsKey("stat")) {
            statistics = (List<Statistics>) dataDTO.get("stat");
        }
        if (dataDTO.containsKey("results")) {
            results = (List<Result>) dataDTO.get("results");
        }
        init();
    }

    private void init() {
        if (statistics != null && !statistics.isEmpty() && statistics.stream().allMatch(Objects::nonNull)) {
            configureChart();
            configureHardestAndEasiest();
            configureWholeValidity();
        } else {
            CourseInfo.showError("Błąd! Nie ma statystyk!");
            statistics.removeIf(Objects::isNull);
        }
        if (results != null && !results.isEmpty() && results.stream().allMatch(Objects::nonNull)) {
            configureGrades();
        }
    }

    private void configureGrades() {

        examAttemptsLabel.setText(examAttemptsLabel.getText() + " " + results.size());
        bestGradeLabel.setText(bestGradeLabel.getText() + " " + StatisticsUtils.getBestGrade(results));
        worstGradeLabel.setText(worstGradeLabel.getText() + " " + StatisticsUtils.getWorstGrade(results));
    }

    private void configureWholeValidity() {
        wholeValiditylabel.setText(wholeValiditylabel.getText() + " " + String.format("%.2f", StatisticsUtils.getValidity(statistics)) + " %");
    }

    private void configureHardestAndEasiest() {
        maxWord = StatisticsUtils.getMostOften(statistics);
        minWord = StatisticsUtils.getLeastOftern(statistics);
        hardestWord.setText(hardestWord.getText() + " " + minWord.getPol());
        easiestWord.setText(easiestWord.getText() + " " + maxWord.getPol());

    }

}
