package pl.hub.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.hub.consts.ModeOfLearning;
import pl.hub.lang.statistics.Statistics;
import pl.hub.lang.practices.AdvancedPractise;
import pl.hub.lang.practices.BasicPractise;
import pl.hub.lang.practices.Practise;
import pl.hub.model.User;
import pl.hub.utils.DataTransferDTO;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

public class PractiseController implements AboveController, Initializable {

    @FXML
    private Label modeLabel;

    @FXML
    private ListView<String> listOfUnknown;

    @FXML
    private ListView<String> listOfKnown;

    @FXML
    private Label presentWord;

    @FXML
    private Label validLabel;

    @FXML
    private Button startbtn;

    @FXML
    private Button exitBtn;

    @FXML
    private TextField userAnswer;

    @FXML
    private Button validateBtn;

    @FXML
    private Label stats;

    @FXML
    private Label grade;

    private Label invisibleFlag;


    private File file;
    private Map<String, String> flashcards;
    private ModeOfLearning mode;
    private Practise practise;
    private DataTransferDTO returnedFromPractise;
    private Statistics statistics;
    private User user;

    @FXML
    void exit(ActionEvent event) {
        ((Stage) (((Node) event.getSource()).getScene().getWindow())).close();
    }

    @FXML
    void start(ActionEvent event) {
        practise = chooseMode();
        if (practise != null) {
            practise.practise();
        }
        startbtn.setDisable(true);
    }

    @FXML
    void validate() {
        // unused...
    }


    private Practise chooseMode() {
        DataTransferDTO dataTransferDTO = new DataTransferDTO();
        dataTransferDTO.add("btn", validateBtn);
        dataTransferDTO.add("txF", userAnswer);
        dataTransferDTO.add("plik", file);
        dataTransferDTO.add("label", presentWord);
        dataTransferDTO.add("unknown", listOfUnknown);
        dataTransferDTO.add("known", listOfKnown);
        dataTransferDTO.add("validLabel", validLabel);
        dataTransferDTO.add("stats", stats);
        dataTransferDTO.add("grade", grade);
        dataTransferDTO.add("flag", invisibleFlag);
        dataTransferDTO.add("user",user);
        return switch (mode) {
            case BASIC -> new BasicPractise(dataTransferDTO);
            case ADVANCED -> new AdvancedPractise(dataTransferDTO);
        };
    }


    @Override
    public DataTransferDTO returnData() {
        DataTransferDTO dataTransferDTO = new DataTransferDTO();
        dataTransferDTO.add("statsPractice", statistics);
        return dataTransferDTO;
    }

    @Override
    public void setInitialData(DataTransferDTO dataDTO) {
        if (dataDTO != null) {
            if (dataDTO.containsKey("plik")) {
                file = new File((String) dataDTO.get("plik"));
            }
            if (dataDTO.containsKey("tryb")) {
                mode = (ModeOfLearning) dataDTO.get("tryb");
            }
            if (dataDTO.containsKey("user")) {
                user = (User) dataDTO.get("user");
            }
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        validLabel.setVisible(false);
        invisibleFlag = new Label();
        invisibleFlag.textProperty().addListener((observable, oldValue, newValue) -> {
            statistics = practise.returnStatsObjectAndSaveData();
        });
    }

}
