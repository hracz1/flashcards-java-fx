package pl.hub.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import pl.hub.dbmanagement.usersDB.UserRepository;
import pl.hub.dbmanagement.usersDB.UserRepositorySecurityDb;
import pl.hub.security.Role;
import pl.hub.utils.CourseInfo;
import pl.hub.consts.ModeOfLearning;
import pl.hub.lang.statistics.Result;
import pl.hub.lang.statistics.Statistics;
import pl.hub.model.User;
import pl.hub.utils.CustomStage;
import pl.hub.utils.DataTransferDTO;

import javax.swing.*;
import java.io.File;
import java.net.URL;
import java.util.*;

public class MenuController implements Initializable {

    private static final String PATH = "/pages/";
    private static final String PRACTISE_FXML = "practise.fxml";
    private static final String EXAM_FXML = "exam.fxml";
    private static final String SETTINGS_FXML = "settings.fxml";
    private static final String STATS_FXML = "stats.fxml";
    private static final String MANAGE_FXML = "flashesmanager.fxml";
    private static final String ADMIN_FXML = "admin.fxml";
    private static final int HEIGHT = 504;
    private static final int WIDTH = 785;

    @FXML
    private GridPane mainPane;

    @FXML
    private Label welcome;

    @FXML
    private Button learnBtn;

    @FXML
    private Button examBtn;

    @FXML
    private Button settingsBtn;

    @FXML
    private Button exitBtn;

    @FXML
    private Button statsBtn;

    @FXML
    private Button fcManagerBtn;
    @FXML
    private Button adminBtnId;
    private User user;
    private File file;
    private ModeOfLearning mode;
    private List<Statistics> statisticsList;
    private List<Result> resultList;

    @FXML
    void exam() {
        if (validatePotentialyNullable()) {
            CourseInfo.showError("Dokonaj konfiguracji!");
            return;
        }
        DataTransferDTO userDTO = new DataTransferDTO();
        userDTO.add("user", this.user);
        userDTO.add("plik", file != null ? file.getAbsolutePath() : "");
        DataTransferDTO results = changeTheWindow(Mode.EXAM, userDTO);
        resultList.add((Result) Objects.requireNonNull(results).get("results"));

    }

    private boolean validatePotentialyNullable() {
        return mode == null || file == null;
    }

    @FXML
    void exit() {
        Platform.exit();
    }

    @FXML
    void learn() {
        if (validatePotentialyNullable()) {
            CourseInfo.showError("Dokonaj konfiguracji!");
            return;
        }
        DataTransferDTO dataTransferDTO = changeTheWindow(Mode.LEARN);
        statisticsList.add((Statistics) Objects.requireNonNull(dataTransferDTO).get("statsPractice"));
    }

    @FXML
    public void openAdminTools() {
        changeTheWindow(Mode.ADMIN);
    }

    @FXML
    void settings() {
        DataTransferDTO dataTransferDTO = changeTheWindow(Mode.SETTINGS);
        setDataFromSettings(dataTransferDTO);
    }

    private void setDataFromSettings(DataTransferDTO dataTransferDTO) {
        if (dataTransferDTO != null) {
            if (dataTransferDTO.containsKey("tryb")) {
                mode = (ModeOfLearning) dataTransferDTO.get("tryb");
            }
            if (dataTransferDTO.containsKey("plik")) {
                file = new File((String) dataTransferDTO.get("plik"));
            }
        }
    }

    @FXML
    void manageFlashcards() {
        changeTheWindow(Mode.MANAGE);
    }

    @FXML
    void showStats() {
        if (validatePotentialyNullable()) {
            CourseInfo.showError("Dokonaj konfiguracji!");
            return;
        }
        DataTransferDTO dataTransferDTO = new DataTransferDTO();
        dataTransferDTO.add("stat", statisticsList);
        dataTransferDTO.add("results", resultList);

        if (statisticsList.isEmpty() && resultList.isEmpty()) {
            CourseInfo.showError("Nie ma jeszcze żadnych statystyk!");
            return;
        }
        changeTheWindow(Mode.STATS, dataTransferDTO);
    }

    public void setInitState(User user) {
        this.user = user;
        checkIfAdmin();
        checkAccountStatus();
        showMessages();
    }

    private void showMessages() {
        user.getMessages().stream()
                .sorted(Comparator.reverseOrder())
                .forEach(s-> new CourseInfo().showAsInstance(s));
    }

    private void checkAccountStatus() {
        if (!user.isActive()) {
            setLock();
            CourseInfo.showError("Niestety Twoje konto zostało zablokowane,\n skontaktuj się z administratorem w celu\n wyjaśnienia przyczyny blokady!");
        }
    }

    private DataTransferDTO changeTheWindow(Mode mode) {
        return changeTheWindow(mode, new DataTransferDTO[]{});
    }

    private DataTransferDTO changeTheWindow(Mode mode, DataTransferDTO... dtos) {

        setLock();
        FXMLLoader fxmlLoader = returnFXMLLoader(mode);
        Parent parent;
        try {
            parent = fxmlLoader.load();
        } catch (Exception e) {
            e.printStackTrace();
            CourseInfo.showError("Kontroler nie istnieje");
            setUnlock();
            return null;
        }
        AboveController aboveController = fxmlLoader.getController();
        Scene scene = new Scene(parent, WIDTH, HEIGHT);
        CustomStage customStage = new CustomStage();
        customStage.setScene(scene);

        if (dtos.length == 0) {
            aboveController.setInitialData(getDTO());
        } else {
            aboveController.setInitialData(DataTransferDTO.joinIntoOneDTO(dtos));
        }

        DataTransferDTO dataTransferDTO = customStage.showAndReturn(aboveController);
        setUnlock();
        return dataTransferDTO;
    }

    private DataTransferDTO getDTO() {
        DataTransferDTO dataTransferDTO = new DataTransferDTO();
        dataTransferDTO.add("tryb", this.mode);
        dataTransferDTO.add("plik", file != null ? file.getAbsolutePath() : "");
        dataTransferDTO.add("user", user);
        return dataTransferDTO;
    }

    private FXMLLoader returnFXMLLoader(Mode mode) {
        String path = switch (mode) {
            case LEARN -> PATH + PRACTISE_FXML;
            case EXAM -> PATH + EXAM_FXML;
            case STATS -> PATH + STATS_FXML;
            case SETTINGS -> PATH + SETTINGS_FXML;
            case MANAGE -> PATH + MANAGE_FXML;
            case ADMIN -> PATH + ADMIN_FXML;
        };
        return new FXMLLoader(getClass().getResource(path));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        statisticsList = new ArrayList<>();
        resultList = new ArrayList<>();
    }

    private void checkIfAdmin() {
        adminBtnId.setVisible(user.getRole() == Role.ADMIN);
    }

    private enum Mode {
        LEARN, EXAM, SETTINGS, STATS, MANAGE, ADMIN;
    }

    private void setLock() {
        learnBtn.setDisable(true);
        examBtn.setDisable(true);
        settingsBtn.setDisable(true);
        statsBtn.setDisable(true);
        fcManagerBtn.setDisable(true);
    }

    private void setUnlock() {
        learnBtn.setDisable(false);
        examBtn.setDisable(false);
        settingsBtn.setDisable(false);
        statsBtn.setDisable(false);
        fcManagerBtn.setDisable(false);
    }


}
