package pl.hub.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.hub.consts.ModeOfLearning;
import pl.hub.lang.flashcards.FlashcardsOperator;
import pl.hub.model.User;
import pl.hub.utils.CourseInfo;
import pl.hub.utils.DataTransferDTO;
import pl.hub.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class SettingsController implements Initializable, AboveController {

    private static final String INITIAL_TEXT = "Wybrany plik";
    private static final String FAILURE = "Niepowodzenie!";

    @FXML
    private GridPane mainPane;

    @FXML
    private Button saveAndExitBtn;

    @FXML
    private ComboBox<ModeOfLearning> modeChoice;

    @FXML
    private Button chooseFileBtn;

    @FXML
    private Button btnGetFromDb;

    @FXML
    private Label choosenFileText;
    private Stage stage;
    private User user;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<ModeOfLearning> observableList = FXCollections.observableList(Arrays.asList(ModeOfLearning.values()));
        modeChoice.setItems(observableList);
    }

    @Override
    public DataTransferDTO returnData() {
        DataTransferDTO dataTransferDTO = new DataTransferDTO();
        dataTransferDTO.add("tryb", modeChoice.getSelectionModel().getSelectedItem());
        if (!INITIAL_TEXT.trim().equals(choosenFileText.getText().trim()) && !FAILURE.trim().equals(choosenFileText.getText().trim())) {
            dataTransferDTO.add("plik", choosenFileText.getText());
        }
        return dataTransferDTO;
    }

    @Override
    public void setInitialData(DataTransferDTO dataDTO) {
        stage = (Stage) mainPane.getScene().getWindow();
        if (dataDTO != null) {
            if (dataDTO.containsKey("user")) {
                user = (User) dataDTO.get("user");
            }
            if (dataDTO.containsKey("tryb")) {
                modeChoice.setValue((ModeOfLearning) dataDTO.get("tryb"));
            }
            if (dataDTO.containsKey("plik")) {
                choosenFileText.setText((String) dataDTO.get("plik"));
            }
        }
        init();
    }

    private void init() {
        long numberOfFlashcards = new FlashcardsOperator(user).getNumberOfFlashcards();
        btnGetFromDb.setText("Pobierz " + numberOfFlashcards + " fiszek dla użytkownika " + user.getLogin());
    }

    @FXML
    void choose() throws IOException {

        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(stage);
        if (!FileUtils.isGoodFormat(file)) {
            CourseInfo.showError("Błąd wyboru pliku!");
            choosenFileText.setText("");
            return;
        }
        choosenFileText.setText(file.getAbsolutePath());

    }

    @FXML
    void saveAndExit() {
        stage.close();
    }

    @FXML
    void downloadFromDb(ActionEvent event) {
        FlashcardsOperator flashcardsOperator = new FlashcardsOperator(user);
        File file = flashcardsOperator.downloadFlashcardsAsFile();
        if (file == null) {
            choosenFileText.setText(FAILURE);
            return;
        }
        choosenFileText.setText(file.getAbsolutePath());
    }
}
