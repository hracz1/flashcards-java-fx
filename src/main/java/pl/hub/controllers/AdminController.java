package pl.hub.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import pl.hub.lang.AdminService;
import pl.hub.utils.CourseInfo;
import pl.hub.utils.DataTransferDTO;

import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.ResourceBundle;

public class AdminController implements AboveController, Initializable {

    @FXML
    private ListView<String> listOfUsers;

    @FXML
    private Label userName;

    @FXML
    private Label userValidity;

    @FXML
    private Label userEffectivityOnExam;

    @FXML
    private Label accountStatus;

    @FXML
    private TextArea message;

    @FXML
    private Button sendMsgId;

    @FXML
    private Button statusIdBtn;

    private AdminService adminService;
    private Map<Integer, String> users;

    @FXML
    void sendMsg() {
        String name = listOfUsers.getFocusModel().getFocusedItem();
        adminService.setMessageForUser(getIdByLogin(name), message.getText());
        CourseInfo.showInfo("Wysłano wiadomość do: " + name);
    }

    @FXML
    void changeStatus() {
        boolean status = adminService.changeStatus(getIdByLogin(listOfUsers.getFocusModel().getFocusedItem()));
        if (status) {
            statusIdBtn.setText("Odblokuj konto");
        } else {
            statusIdBtn.setText("Wygaś konto");
        }
        listOfUsers.fireEvent(new Event(MouseEvent.MOUSE_CLICKED));
    }

    @Override
    public DataTransferDTO returnData() {
        //unused
        return null;
    }

    @Override
    public void setInitialData(DataTransferDTO dataDTO) {
        //unused
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        adminService = new AdminService();
        users = adminService.getUsersNames();

        ObservableList<String> data = FXCollections.observableArrayList(new ArrayList<>(users.values()));
        listOfUsers.setItems(data);

        listOfUsers.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            String name = listOfUsers.getFocusModel().getFocusedItem();
            boolean userStatus = adminService.getUserStatusById(getIdByLogin(name));

            userName.setText("Użytkownik: " + name);
            accountStatus.setText("Status konta : " + (userStatus ? "aktywne" : "nieaktywne"));
            userValidity.setText(calcValidity(name));
            userEffectivityOnExam.setText(calcExamEffectivity(name));
            statusIdBtn.setText(userStatus ? "Wygaś konto" : "Odblokuj konto");
        });

    }

    private String calcExamEffectivity(String name) {
        Map<String, Integer> examResultsById = adminService.getExamResultsById(getIdByLogin(name));
        int positive = examResultsById.get("positive_exam_attempts");
        int all = examResultsById.get("exam_attempts");
        return positive + "/" + all + " wyników pozytywnych";
    }

    private String calcValidity(String name) {
        Map<String, Integer> userValidityById = adminService.getUserValidityById(getIdByLogin(name));
        int good = userValidityById.get("good");
        int all = userValidityById.get("all_fc");
        return String.format("Poprawność: %.2f", (all != 0 ? (100.0 * good / all) : 0)) + "%";
    }

    private int getIdByLogin(String login) {
        for (Map.Entry<Integer, String> entry : users.entrySet()) {
            if (entry.getValue().equals(login)) {
                return entry.getKey();
            }
        }
        return -1;
    }
}
