package pl.hub.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.hub.lang.decorators.Decorator;
import pl.hub.lang.decorators.DecoratorsFactory;
import pl.hub.lang.flashcards.FlashcardsOperator;
import pl.hub.model.User;
import pl.hub.utils.CourseInfo;
import pl.hub.utils.DataTransferDTO;
import pl.hub.utils.FileUtils;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlashcardsController implements AboveController {

    @FXML
    private ListView<String> table;

    @FXML
    private Label mode;

    @FXML
    private TextField tfPolish;

    @FXML
    private TextField tfEnglish;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSortAlf;

    @FXML
    private Button btnShuffle;

    @FXML
    private ComboBox<String> cbDecoration;

    @FXML
    private CheckBox chbSave;

    @FXML
    private Button btnDecorate;

    @FXML
    private Button btnSaveAndClose;

    @FXML
    private Button btnCancelAndClose;

    private User user;
    private FlashcardsOperator flashcardsOperator;
    private File file;
    private FlashcardContext context;
    private Map<String, String> map;
    private Decorator decorator;
    private Map<String, String> copyOfMap;

    @FXML
    void add() {
        if (tfPolish.getText() == null || tfEnglish.getText() == null) {
            return;
        }
        if (context == FlashcardContext.ADD) {
            map.put(tfPolish.getText(), tfEnglish.getText());
            table.setItems(null);
            refreshTable();
        } else {
            map.replace(tfPolish.getText(), tfEnglish.getText());
        }
        changeContext(true);
    }

    private void refreshTable() {
        table.setItems(FXCollections.observableArrayList(new ArrayList<>(map.keySet())));
    }

    @FXML
    void cancel() {
        changeContext(true);
    }

    @FXML
    void cancelAndClose() {
        if (copyOfMap != null)
            map = new LinkedHashMap<>(copyOfMap);
        if (map != null)
            copyOfMap = new LinkedHashMap<>(map);
        refreshTable();
        ((Stage) mode.getScene().getWindow()).close();
    }

    @FXML
    void chooseDecorator() {
        Stream.of(DecoratorsFactory.DecoratorType.values())
                .filter(s -> s.getType().equals(cbDecoration.getSelectionModel().getSelectedItem()))
                .findFirst()
                .ifPresent(s -> decorator = DecoratorsFactory.createDecorator(s));
    }

    @FXML
    void decorate() {
        if (decorator == null) {
            return;
        }
        map = new LinkedHashMap<>(decorator.decorate(map));
        refreshTable();
    }

    @FXML
    void delete() {
        if (tfPolish.getText() == null || tfEnglish.getText() == null) {
            return;
        }
        map.remove(tfPolish.getText());
        changeContext(true);
        refreshTable();

    }

    @FXML
    void saveAndClose() {
        boolean saveToDb = chbSave.isSelected();
        if (saveToDb) {
            flashcardsOperator.save(map, user, file);
        } else {
            flashcardsOperator.save(map, file);
        }
        ((Stage) mode.getScene().getWindow()).close();
    }

    @FXML
    void shuffle() {
        if (map == null || map.isEmpty()) {
            return;
        }

        List<String> preparedToShuffleList = map.entrySet().stream()
                .map(s -> s.getKey() + "-" + s.getValue())
                .collect(Collectors.toList());

        Collections.shuffle(preparedToShuffleList);

        map = preparedToShuffleList.stream()
                .collect(Collectors.toMap(s -> s.split("-")[0], s -> s.split("-")[1], (s1, s2) -> s1, LinkedHashMap::new));

        refreshTable();
    }

    @FXML
    void sort() {
        map = new TreeMap<>(map);
        refreshTable();
    }

    @Override
    public DataTransferDTO returnData() {
        return null;
    }

    @Override
    public void setInitialData(DataTransferDTO dataDTO) {
        if (dataDTO != null) {
            if (dataDTO.containsKey("user")) {
                user = (User) dataDTO.get("user");
            }
            if (dataDTO.containsKey("plik")) {
                file = new File((String) dataDTO.get("plik"));
            }
        }
        init();
        initListeners();
    }

    @FXML
    void setSaveInDb() {

    }

    private void initListeners() {
        table.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            changeContext(false);

            if (map != null) {
                tfPolish.setText(table.getSelectionModel().getSelectedItem());
                tfEnglish.setText(map.get(table.getSelectionModel().getSelectedItem()));
            }
        });
    }

    private void changeContext(boolean cond) {
        if (cond) {
            mode.setText("Tryb: dodawanie");
            tfPolish.setText("");
            tfEnglish.setText("");
            context = FlashcardContext.ADD;
            btnAdd.setText("Dodaj");
            tfPolish.setDisable(false);
        } else {
            mode.setText("Tryb: modyfikacja");
            context = FlashcardContext.EDIT;
            btnAdd.setText("Modyfikuj");
            tfPolish.setDisable(true);
        }
    }

    private void init() {
        flashcardsOperator = new FlashcardsOperator(user);
        map = FileUtils.getFlashesAsMap(file);
        if (map == null) {
            CourseInfo.showError("Mapa fiszek nie może został utworzona!");
            map = new LinkedHashMap<>();
            return;
        }
        copyOfMap = new LinkedHashMap<>(map);
        changeContext(true);
        refreshTable();
        ObservableList<String> observableList = FXCollections.observableList(
                Stream.of(DecoratorsFactory.DecoratorType.values()).map(DecoratorsFactory.DecoratorType::getType).collect(Collectors.toList()));
        cbDecoration.setItems(observableList);
        cbDecoration.setValue("Nie wybrano");
    }

    private enum FlashcardContext {
        ADD, EDIT
    }
}
