package pl.hub.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import pl.hub.model.User;
import pl.hub.security.UserLogger;
import pl.hub.security.UserRegistry;
import pl.hub.utils.CourseInfo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML
    private Button loginBtn;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passField;

    @FXML
    private Hyperlink registerLink;

    @FXML
    void login() throws IOException {
        User user = findUser();

        if (user == null ) {
            reset();
            return;
        }

        changeTheWindow(user);
    }

    private void reset() {
        loginField.setText("Wprowadź poprawne dane!");
        passField.setText("");
    }

    private void changeTheWindow(User user) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/pages/menu.fxml"));
        Parent parent = fxmlLoader.load();

        Stage mainStage = (Stage) loginBtn.getScene().getWindow();
        mainStage.setScene(new Scene(parent, 942, 595));
        MenuController menuController = fxmlLoader.getController();
        menuController.setInitState(user);
        mainStage.show();

    }

    private User findUser() {
        return new UserLogger(loginField.getText(), passField.getText()).tryToLogin();

    }

    public void register(){
        UserRegistry userRegistry = new UserRegistry();
        if(loginField.getText() == null || passField.getText() == null || passField.getText().isEmpty() || loginField.getText().isEmpty()){
            CourseInfo.showError("Podano wadliwe dane!");
            return;
        }
        registerLink.setText("Trwa rejestracja...");
        boolean succeeded = userRegistry.register(loginField.getText(),passField.getText());
        if(succeeded){
            registerLink.setText("Pomyślna rejestracja!");
            registerLink.setStyle("-fx-color-label-visible: green");
        }else{
            registerLink.setText("Użytkownik istnieje! Ponów...");
            registerLink.setStyle("-fx-color-label-visible: red");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginField.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode().getName().equals("Enter")) {
                try {
                    login();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        passField.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode().getName().equals("Enter")) {
                try {
                    login();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
