package pl.hub.security;

import pl.hub.dbmanagement.usersDB.UserRepository;
import pl.hub.dbmanagement.usersDB.UserRepositorySecurityDb;
import pl.hub.lang.mailing.MailFactory;
import pl.hub.lang.mailing.MailService;
import pl.hub.utils.CourseInfo;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

public class PasswordManager {
    private static final String ALGORITHM = "PBKDF2WithHmacSHA512";
    private static final int NUMBER_OF_ITERATIONS = 4096; //powinno być 2^16 czyli = 65536, ale zbyt dlugo to trwa na ogól
    private static final int KEY_LENGTH = 256;
    private static PasswordManager instance = null;

    private Salt salt;
    private final SaltRepository saltRepository = new SaltRepositoryImpl();
    private final UserRepository userRepository = new UserRepositorySecurityDb();

    private PasswordManager() {
        configSalt();
    }

    private void configSalt() {
        salt = Salt.tryToParse(saltRepository.getSalt());

        if (salt == null) {
            salt = new Salt(16);
            salt.generate();
            storeGeneratedSalt();
        }
    }

    public static PasswordManager getInstance() {
        if (instance == null) {
            instance = new PasswordManager();
        }
        return instance;
    }

    public String encodePassword(String pass) {
        char[] passAsArray = pass.toCharArray();

        KeySpec spec = new PBEKeySpec(pass.toCharArray(), salt.getValueBytes(), NUMBER_OF_ITERATIONS, KEY_LENGTH);

        Arrays.fill(passAsArray, Character.MIN_VALUE);

        SecretKeyFactory factory;
        try {
            factory = SecretKeyFactory.getInstance(ALGORITHM);
            final byte[] encoded = factory.generateSecret(spec).getEncoded();
            return Base64.getEncoder().encodeToString(encoded);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        CourseInfo.showError("Błąd podczas zabezpieczania hasła! Skontaktuj się z administratorem!");
        return null;
    }

    private void storeGeneratedSalt() {
        saltRepository.saveSalt(salt);
    }

    public boolean verify(String login, String password) {
        String encodedFromDb = userRepository.getEncodedPassword(login);

        String provided = encodePassword(password);

        return provided.equals(encodedFromDb);
    }

    public boolean tryToLoginAsAdmin() {
        AdminAuthorization adminAuthorization = new AdminAuthorization();
        return adminAuthorization.showDialogAndVerify();
    }

    private static class AdminAuthorization {
        private static final int LENGTH_OF_TOKEN = 32;

        private final String generatedToken;

        public AdminAuthorization() {
            this.generatedToken = generateToken();
            sendToken();
        }

        private String generateToken() {
            SecureRandom secureRandom = new SecureRandom();
            byte[] bytes = new byte[LENGTH_OF_TOKEN];
            secureRandom.nextBytes(bytes);
            return Base64.getEncoder().encodeToString(bytes);
        }

        private boolean showDialogAndVerify() {
            AdminCredentailsInputDialog adminAuthWindow = new AdminCredentailsInputDialog();
            String answer = adminAuthWindow.createDialog();
            return generatedToken.equals(answer);
        }

        private void sendToken() {
            MailService mail = MailFactory.createSender("Wyslany token : " + generatedToken,"flashcardscourseturbo321@gmail.com");
            mail.send();
        }
    }
}
