package pl.hub.security;

import pl.hub.dbmanagement.usersDB.*;
import pl.hub.model.User;

public class UserRegistry {

    private final UserRepository userRepository;
    private final UserStatsRepository userStatsRepository;

    public UserRegistry() {
        userRepository = new UserRepositoryDb();
        userStatsRepository = new UserStatsRepositoryDb();
    }

    public boolean register(String login, String pass) {
        User user = new User(login, encodePassword(pass));
        boolean result =  userRepository.saveUser(user);
        if(result){
            UserAdditionalTooler userAdditionalTooler = new UserAdditionalTooler(user);
            user = userAdditionalTooler.fillId();
            userStatsRepository.insertStatsRecord(user.getId());
        }
        return result;
    }

    private String encodePassword(String pass){
        return PasswordManager.getInstance().encodePassword(pass);
    }
}
