package pl.hub.security;

import java.util.Map;

public interface SaltRepository {
    Map<String, Object> getSalt();
    void saveSalt(Salt salt);
}
