package pl.hub.security;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import pl.hub.controllers.AboveController;
import pl.hub.lang.timer.TimerExecutor;
import pl.hub.utils.CustomStage;
import pl.hub.utils.DataTransferDTO;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AdminCredentailsInputDialog implements AboveController {

    private TextField textField;
    private CustomStage stage;
    private HBox hBox;
    private Button button;
    private Label time;
    private String text;

    @Override
    public DataTransferDTO returnData() {
        DataTransferDTO dataTransferDTO = new DataTransferDTO();
        dataTransferDTO.add("input", text);
        return dataTransferDTO;
    }

    @Override
    public void setInitialData(DataTransferDTO dataDTO) {
        //unused
    }

    public String createDialog() {
        this.textField = new TextField();
        this.hBox = new HBox();
        this.button = new Button();
        this.button.setText("Spróbuj!");
        this.time = new Label("5");

        prepareListeners();
        prepareHbox();
        prepareTimer();
        DataTransferDTO dataTransferDTO = prepareWindow();
        return (String) dataTransferDTO.get("input");
    }

    private void prepareListeners() {
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            text = textField.getText();
            stage.close();
        });
        time.textProperty().addListener((observableValue, oldValue, newValue) -> {
            if ("Czas minął".equals(newValue)) {
                stage.close();
            }
        });
    }

    private void prepareTimer() {
        TimerExecutor timerExecutor = new TimerExecutor(120);
        time.textProperty().bind(timerExecutor.messageProperty());
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(timerExecutor);
    }

    private DataTransferDTO prepareWindow() {
        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(hBox);
        Scene scene = new Scene(stackPane, 450, 80);
        stage = new CustomStage();
        stage.setScene(scene);
        return stage.showAndReturn(this);
    }

    private void prepareHbox() {
        hBox.setSpacing(10);
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(new Label("Wpisz token"));
        hBox.getChildren().add(textField);
        hBox.getChildren().add(button);
        hBox.getChildren().add(time);
    }
}
