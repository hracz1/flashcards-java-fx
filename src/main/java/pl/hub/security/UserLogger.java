package pl.hub.security;

import pl.hub.dbmanagement.usersDB.UserAdditionalTooler;
import pl.hub.dbmanagement.usersDB.UserRepository;
import pl.hub.dbmanagement.usersDB.UserRepositorySecurityDb;
import pl.hub.model.User;
import pl.hub.utils.CourseInfo;

public class UserLogger {
    private final String login;
    private final String password;
    private final AdminDetector adminDetector;

    public UserLogger(String login, String password) {
        this.login = login;
        this.password = password;
        this.adminDetector = new AdminDetector();
    }

    public User tryToLogin() {
        PasswordManager pm = PasswordManager.getInstance();
        if(!pm.verify(login,password)){
            return null;
        }

        User user = new User(login,password);
        UserAdditionalTooler userAdditionalTooler = new UserAdditionalTooler(user);
        user = userAdditionalTooler.fillId();
        user.setActive(userAdditionalTooler.isActive());
        user = userAdditionalTooler.fillMessages();
        if(adminDetector.detect(user.getLogin())){
            CourseInfo.showInfo("Wykryto próbę zalogowania jako admin!");
            boolean result = adminDetector.tryToLogin();
            user.setRole(Role.ADMIN);
            return result ? user : null;
        }
        return user;
    }

    private static class AdminDetector{
        private final UserRepository userRepository;

        private AdminDetector() {
            this.userRepository = new UserRepositorySecurityDb();
        }

        public boolean detect(String login) {
            Role role = userRepository.getUserRole(login);
            return role == Role.ADMIN;
        }

        public boolean tryToLogin() {
            PasswordManager pm = PasswordManager.getInstance();
            return pm.tryToLoginAsAdmin();
        }
    }
}
