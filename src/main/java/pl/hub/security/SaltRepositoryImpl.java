package pl.hub.security;

import pl.hub.dbmanagement.connection.AbstractRepo;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class SaltRepositoryImpl extends AbstractRepo implements SaltRepository {
    public SaltRepositoryImpl() {
        super();
    }

    @Override
    public Map<String, Object> getSalt() {
        String query = "" +
                "select " +
                "  * " +
                "from " +
                "  salts";
        List<Map<String, Object>> resultAsList = getResultAsList(query);
        return resultAsList.isEmpty() ? null : resultAsList.get(0);
    }

    @Override
    public void saveSalt(Salt salt) {
        String query = "" +
                "insert into " +
                "  salts (value,bytes,length) " +
                "values " +
                "  ('" + salt.getValueAsString() + "','" + Arrays.toString(salt.getValueBytes()) + "',"+salt.getLength()+")";
        updateData(query);
    }
}
