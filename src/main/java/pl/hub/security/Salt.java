package pl.hub.security;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Map;

public class Salt {
    private static final SecureRandom RANDOM = new SecureRandom();
    private final int length;

    private String value;
    private byte[] valueBytes;

    public Salt(int length) {
        this.length = length;
    }

    public static Salt tryToParse(Map<String, Object> props) {
        if(props == null || props.isEmpty()){
            return null;
        }
        Salt salt = new Salt((Integer)props.get("length"));
        salt.value = (String)props.get("value");
        String bytes = (String)props.get("bytes");
        salt.valueBytes = bytes.getBytes();
        return salt;
    }

    public void generate() {
        byte[] bytes = new byte[length];
        RANDOM.nextBytes(bytes);
        valueBytes = bytes;
        value = Base64.getEncoder().encodeToString(bytes);
    }

    public String getValueAsString() {
        return value;
    }

    public byte[] getValueBytes() {
        return valueBytes;
    }

    public int getLength() {
        return length;
    }
}
