package pl.hub.lang.mailing;

import pl.hub.utils.CourseInfo;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MailService {
    private final MailConfig mailConfig;
    private final String target;
    private final String messageObj;

    public MailService(MailConfig mailConfigurator, String target, String message) {
        this.mailConfig = mailConfigurator;
        this.target = target;
        this.messageObj = message;
    }

    public void send() {
        try {
            Message message = new MimeMessage(mailConfig.getSession());
            message.setFrom(new InternetAddress(mailConfig.getUsername()));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(target));
            message.setSubject("Token");

            message.setContent(messageObj,"text/plain");
            ExecutorService service = Executors.newCachedThreadPool();
            service.execute(new MailSendingExecutor(message));

            service.shutdown();
            CourseInfo.showInfo("Udało się wysłać maila!!");
        } catch (MessagingException e) {
            CourseInfo.showError("Błąd!");
            e.printStackTrace();
        }
    }

}


