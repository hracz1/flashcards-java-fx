package pl.hub.lang.mailing;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;


// oczywiscie jest to bardzo niski poziom, najchetniej bym uzyl spring mail api lub jakarta.
public class MailConfig {
    private final Properties prop;
    private final Session session;

    private final String username = "flashcardscourseturbo321@gmail.com";
    private final String password = "testowy123";

    public MailConfig() {
        prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");

        session = Session.getInstance(prop, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
    }

    public Session getSession() {
        return session;
    }

    public String getUsername() {
        return username;
    }
}
