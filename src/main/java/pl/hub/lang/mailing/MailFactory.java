package pl.hub.lang.mailing;

import pl.hub.lang.statistics.Result;

import java.util.Properties;

public abstract class MailFactory {
    public static MailService createSender(String obj,String target){
        return new MailService(new MailConfig(),target,obj);
    }
}
