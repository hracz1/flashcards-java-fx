package pl.hub.lang.mailing;

import javafx.concurrent.Task;

import javax.mail.Message;
import javax.mail.Transport;

public class MailSendingExecutor extends Task<Boolean> {

    private final Message msg;

    public MailSendingExecutor(Message msg) {
        this.msg = msg;
    }

    @Override
    protected Boolean call() throws Exception {
        Transport.send(msg);
        Thread.sleep(3000);
        return true;
    }
}
