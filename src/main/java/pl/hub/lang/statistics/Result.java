package pl.hub.lang.statistics;

public class Result implements GenStats {
    private String resultAsPercent;
    private String certificationPattern;
    private String grade;

    public Result(String resultAsPercent, String certificationPattern, String grade) {
        this.resultAsPercent = resultAsPercent;
        this.certificationPattern = certificationPattern;
        this.grade = grade;
    }

    public String getResultAsPercent() {
        return resultAsPercent;
    }

    public void setResultAsPercent(String resultAsPercent) {
        this.resultAsPercent = resultAsPercent;
    }

    public String getCertificationPattern() {
        return certificationPattern;
    }

    public String getGrade() {
        return grade;
    }
}
