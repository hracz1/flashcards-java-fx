package pl.hub.lang.statistics;

import pl.hub.lang.practices.Word;
import pl.hub.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Statistics implements GenStats {

    private final List<Word> listOfWords;
    private int counterGood;
    private int counter;
    private double atAll;

    public Statistics(File file) {
        listOfWords = new ArrayList<>();
        parse(file);
    }

    private void parse(File file) {
        Map<String, String> flashesAsMap = FileUtils.getFlashesAsMap(file);
        if (flashesAsMap != null) {
            flashesAsMap.forEach((k, v) -> listOfWords.add(new Word(k, v)));
        }
    }


    public void updateStats(String key, boolean valid) {
        if (valid) {
            listOfWords.stream()
                    .filter(s -> key.equals(s.getPol()))
                    .findFirst()
                    .ifPresent(s -> s.setGood(s.getGood() + 1));
        } else {
            listOfWords.stream()
                    .filter(s -> key.equals(s.getPol()))
                    .findFirst()
                    .ifPresent(s -> s.setBad(s.getBad() + 1));
        }
        sumAll(valid);
    }

    private void sumAll(boolean valid) {
        if (valid) {
            counterGood++;
        }
        counter++;

        atAll = 100.0 * counterGood / counter;
    }

    public double getAtAll() {
        return atAll;
    }

    public String estimateGrade() {
        if (atAll > 50 && atAll <= 65) {
            return "C";
        }
        if (atAll > 65 && atAll <= 85) {
            return "B";
        }
        if (atAll > 85) {
            return "A";
        }
        return "F";
    }

    public List<Word> getListOfWords(){
        return listOfWords;
    }

    public int getCounterGood() {
        return counterGood;
    }

    public int getAllCounter() {
        return counter;
    }
}
