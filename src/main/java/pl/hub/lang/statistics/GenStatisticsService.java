package pl.hub.lang.statistics;

import pl.hub.dbmanagement.usersDB.UserStatsRepository;
import pl.hub.dbmanagement.usersDB.UserStatsRepositoryDb;
import pl.hub.model.User;

public class GenStatisticsService {

    private GenStats genStats;
    private User user;
    private final UserStatsRepository userRepository;

    public GenStatisticsService() {
        this.userRepository = new UserStatsRepositoryDb();
    }

    public GenStatisticsService(GenStats genStats, User user) {
        this.genStats = genStats;
        this.user = user;
        userRepository = new UserStatsRepositoryDb();
    }

    public void saveStats() {
        userRepository.saveStatistics((Statistics) genStats, user.getId());
    }

    public void saveResults() {
        userRepository.saveExamStatistics((Result) genStats, user.getId());
    }

    public void setGenStats(GenStats genStats) {
        this.genStats = genStats;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
