package pl.hub.lang;

import pl.hub.dbmanagement.admin.AdminRepository;
import pl.hub.dbmanagement.admin.AdminRepositoryDb;
import pl.hub.dbmanagement.messages.MessageRepository;
import pl.hub.dbmanagement.messages.MessageRepositoryDb;

import java.util.Map;

public class AdminService {

    private final AdminRepository adminRepository;
    private final MessageRepository messageRepository;

    public AdminService() {
        this.messageRepository = new MessageRepositoryDb();
        this.adminRepository = new AdminRepositoryDb();
    }

    public Map<Integer, String> getUsersNames() {
        return adminRepository.getMapOfUsersNamesWithId();
    }

    public boolean getUserStatusById(int userId) {
        return adminRepository.getAccountStatusByUserId(userId);
    }

    public Map<String, Integer> getUserValidityById(int userId) {
        return adminRepository.getValidityByUserId(userId);
    }

    public Map<String, Integer> getExamResultsById(int userId) {
        return adminRepository.getExamResultsByUserId(userId);
    }

    public boolean changeStatus(int userId) {
        boolean accountStatus = adminRepository.getAccountStatusByUserId(userId);
        if (accountStatus) {
            adminRepository.lockAccount(userId);
            return true;
        } else {
            adminRepository.unlockAccount(userId);
            return false;
        }
    }

    public void setMessageForUser(int idByLogin, String text) {
        messageRepository.saveMessageForUser(text, idByLogin);
    }
}
