package pl.hub.lang.exam;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import pl.hub.dbmanagement.usersDB.UserRepository;
import pl.hub.dbmanagement.usersDB.UserStatsRepository;
import pl.hub.dbmanagement.usersDB.UserStatsRepositoryDb;
import pl.hub.lang.statistics.GenStatisticsService;
import pl.hub.utils.CourseInfo;
import pl.hub.lang.statistics.Result;
import pl.hub.model.User;
import pl.hub.utils.DataTransferDTO;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class ExamMaker {

    private final Exam exam;
    private final User user;
    private final GenStatisticsService genStatisticsService;
    private Label flag;
    private TextField userAnswer;
    private Label presentWordLabel;
    private Label remainingQuestions;

    public ExamMaker(DataTransferDTO dataTransferDTO) {
        this.exam = new Exam((File) dataTransferDTO.get("file"), (User) dataTransferDTO.get("user"));
        this.userAnswer = (TextField) dataTransferDTO.get("userAnswer");
        this.presentWordLabel = (Label) dataTransferDTO.get("presentWordLabel");
        this.remainingQuestions = (Label) dataTransferDTO.get("remainingQuestions");
        this.flag = (Label) dataTransferDTO.get("flag");
        this.user = (User) dataTransferDTO.get("user");
        this.genStatisticsService = new GenStatisticsService();
        setListeners();
        Map.Entry<String, String> entry = exam.next();
        presentWordLabel.setText("Słówko: " + entry.getKey());
    }

    private void setListeners() {
        userAnswer.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode().getName().equals("Enter")) {
                remainingQuestions.setText("Pozostało pytań: " + exam.numberOfRemaining());
                progressExam();
                userAnswer.setText("");
            }
        });
    }

    private void progressExam() {
        exam.processAnswer(userAnswer.getText());
        if (exam.hasNext()) {
            Map.Entry<String, String> entry = exam.next();
            presentWordLabel.setText("Słówko: " + entry.getKey());
        } else {
            flag.setText("DONE!");
        }
    }

    public void executeExam() {
        if (!validate()) {
            CourseInfo.showError("Nastąpił błąd!");
        }
    }

    private boolean validate() {
        return userAnswer != null && presentWordLabel != null && remainingQuestions != null;
    }

    public Result saveAndGetResultObject() throws IOException {
        Result result = exam.returnResults();
        genStatisticsService.setUser(user);
        genStatisticsService.setGenStats(result);
        genStatisticsService.saveResults();
        return result;
    }
}
