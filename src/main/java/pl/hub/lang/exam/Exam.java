package pl.hub.lang.exam;

import pl.hub.consts.PatternCert;
import pl.hub.lang.statistics.Result;
import pl.hub.model.User;
import pl.hub.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

public class Exam {
    private static int i;

    private final File file;
    private Map<String, String> flashcards;
    private double result;
    private boolean isPassed;
    private final User infoAboutStudent;
    private Iterator<Map.Entry<String, String>> entryIterator;
    private Map.Entry<String, String> presentPair;


    public Exam(File file, User user) {
        i = 0;
        this.file = file;
        this.infoAboutStudent = user;
        configure();
    }

    private void configure() {
        flashcards = FileUtils.getFlashesAsMap(file);
        if (flashcards != null) {
            entryIterator = flashcards.entrySet().iterator();
        }
    }

    public Map.Entry<String, String> next() {
        presentPair = entryIterator.next();
        return presentPair;
    }

    public boolean hasNext() {
        return entryIterator.hasNext();
    }

    public void processAnswer(String answer) {
        if (presentPair.getValue().equals(answer)) {
            result++;
        }
    }

    private String modifyPattern() {
        String copy;

        if (isPassed) {
            copy = PatternCert.winPattern;
            copy = copy.replace(":name", infoAboutStudent.getLogin());
            copy = copy.replace(":results", String.format("%.2f", percents()) + " %");
            copy = copy.replace(":grade", calcGrade());
        } else {
            copy = PatternCert.losePattern;
            copy = copy.replace(":nameSurname", infoAboutStudent.getLogin());
            copy = copy.replace(":results", String.format("%.2f", percents()) + " %");
        }
        return copy;

    }

    private String calcGrade() {
        double result = percents();
        if (result > 50 && result <= 66) {
            return "C";
        }
        if (result > 66 && result <= 85) {
            return "B";
        }
        if (result > 85) {
            return "A";
        }
        return "F";
    }

    public Result returnResults() {
        return new Result(String.format("%f", percents()), modifyPattern(), calcGrade());
    }

    private double percents() {
        double percents = 100 * result / flashcards.size();
        isPassed = percents > 50;
        return percents;
    }

    public int numberOfRemaining() {
        int sub = flashcards.size() - ++i;
        if (sub == 0) {
            i = 0;
        }
        return sub;
    }
}
