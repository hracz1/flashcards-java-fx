package pl.hub.lang.decorators;

import java.util.LinkedHashMap;
import java.util.Map;

public class LowerCaseDecorator implements Decorator {
    @Override
    public Map<String, String> decorate(Map<String, String> map) {
        Map<String, String> newMap = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            newMap.put(entry.getKey().toLowerCase(), entry.getValue().toLowerCase());
        }
        return newMap;
    }
}
