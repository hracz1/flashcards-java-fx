package pl.hub.lang.decorators;

import java.io.File;
import java.util.Map;

@FunctionalInterface
public interface Decorator {
    Map<String,String> decorate(Map<String, String> map);

}
