package pl.hub.lang.decorators;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MixedCaseDecorator implements Decorator {
    @Override
    public Map<String, String> decorate(Map<String, String> map) {
        Map<String, String> newMap = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            newMap.put(getMixed(entry.getKey()), getMixed(entry.getValue()));
        }
        return newMap;
    }

    private String getMixed(String word) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            if (i % 2 == 0) {
                result.append(String.valueOf(word.charAt(i)).toLowerCase());
            } else {
                result.append(String.valueOf(word.charAt(i)).toUpperCase());
            }
        }
        return result.toString();
    }

}
