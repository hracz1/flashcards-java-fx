package pl.hub.lang.decorators;

public abstract class DecoratorsFactory {
    public static Decorator createDecorator(DecoratorType decoratorType) {
        return switch (decoratorType) {
            case UPPER -> new UpperCaseDecorator();
            case MIXED_CASE -> new MixedCaseDecorator();
            default -> new LowerCaseDecorator();
        };
    }

    @SuppressWarnings("unused")
    public enum DecoratorType {
        UPPER("Wielkie litery"),
        LOWER("Małe litery"),
        MIXED_CASE("Mieszane litery");

        private final String type;

        DecoratorType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }
}
