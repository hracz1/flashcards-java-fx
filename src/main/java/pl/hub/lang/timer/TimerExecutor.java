package pl.hub.lang.timer;

import javafx.concurrent.Task;

public class TimerExecutor extends Task<Void> {
    private int time;

    public TimerExecutor(int time) {
        this.time = time;
    }

    @Override
    protected Void call() {
        while (true) {
            if (time == 0) {
                updateMessage("Czas minął");
                return null;
            }
            updateMessage("Pozostało czasu: " + time-- + " sekund");
            try {
                Thread.sleep(1000); // jest warning, ale o to chodzi by było busy waiting moim zdaniem
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
