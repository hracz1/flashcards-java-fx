package pl.hub.lang.flashcards;

import pl.hub.dbmanagement.flashcards.FlashcardsRepository;
import pl.hub.dbmanagement.flashcards.FlashcardsRepositoryDb;
import pl.hub.model.User;
import pl.hub.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class FlashcardsOperator {
    private final FlashcardsRepository flashcardsRepository;
    private final User user;

    public FlashcardsOperator(User user) {
        this.user = user;
        flashcardsRepository = new FlashcardsRepositoryDb();
    }

    public List<Map<String, String>> downloadFlashcardsAsMap() {
        return flashcardsRepository.getFlashes(user);
    }

    public File downloadFlashcardsAsFile() {
        try {
            File file = File.createTempFile("tmp", ".tmp");
            file.deleteOnExit();
            List<Map<String, String>> listMap = downloadFlashcardsAsMap();
            if (listMap == null) {
                return null;
            }
            FileUtils.saveToTempFile(file, listMap);
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void save(Map<String, String> map, User user, File file) {
        save(map, file);
        flashcardsRepository.updateOrInsertFlashcards(map, user);
    }

    public void save(Map<String, String> map, File file) {
        FileUtils.saveToFile(file, map);
    }

    public long getNumberOfFlashcards() {
        return flashcardsRepository.getNumberOfFlashes(user);
    }
}
