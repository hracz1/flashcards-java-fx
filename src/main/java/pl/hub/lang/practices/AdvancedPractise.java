package pl.hub.lang.practices;

import pl.hub.utils.CourseInfo;
import pl.hub.utils.DataTransferDTO;
import pl.hub.utils.FileUtils;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class AdvancedPractise extends AbstractPractise {
    private final Map<String, Integer> knownWords = new LinkedHashMap<>();
    private Map<String, String> copy;
    private Iterator<Map.Entry<String, String>> iterator;

    public AdvancedPractise(DataTransferDTO dataTransferDTO) {
        super(dataTransferDTO);
    }

    @Override
    public void practise() {
        Map<String, String> orgMap = FileUtils.getFlashesAsMap(file);
        copy = new LinkedHashMap<>(orgMap);
        iterator = copy.entrySet().iterator();

    }

    @Override
    protected void loopOver() {
        if (iterator.hasNext()) {
            Map.Entry<String, String> actual = iterator.next();
            String actualKey = actual.getKey();
            String actualValue = actual.getValue();

            String valueFromUser = textField.getText();
            if (actualValue.equals(valueFromUser)) {
                knownWords.put(actualKey, knownWords.getOrDefault(actualKey, 0) + 1);
                setInfo(true, actualKey + " - Poprawnie  " + knownWords.get(actualKey) + "/3", actualKey + " - Niepoprawnie!");
                if (knownWords.get(actualKey) == 3) {
                    fillKnown();
                    iterator.remove();
                }
                updateStats(actualKey, true);
            } else {
                updateStats(actualKey, false);
                setInfo(false, actualKey + " - Poprawnie  " + knownWords.get(actualKey) + "/3", actualKey + " - Niepoprawnie!");
            }

            if (!iterator.hasNext()) {
                iterator = copy.entrySet().iterator();
            }
        } else {
            CourseInfo.showInfo("Ukończyłeś praktykę kursu rozszerzonego!");
        }
    }
}
