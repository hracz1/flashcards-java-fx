package pl.hub.lang.practices;

import pl.hub.utils.CourseInfo;
import pl.hub.utils.DataTransferDTO;
import pl.hub.utils.FileUtils;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class BasicPractise extends AbstractPractise {
    private Map<String, String> orgMap;
    private Map<String, String> copy;
    private Iterator<Map.Entry<String, String>> iterator;

    public BasicPractise(DataTransferDTO dataTransferDTO) {
        super(dataTransferDTO);
    }

    @Override
    public void practise() {
        orgMap = FileUtils.getFlashesAsMap(file);
        copy = new LinkedHashMap<>(orgMap);
        iterator = copy.entrySet().iterator();
    }

    @Override
    protected void loopOver() {
        if (iterator.hasNext() && copy.size() > orgMap.size() / 2) {
            Map.Entry<String, String> actual = iterator.next();
            String actualValue = actual.getValue();
            String valueFromUser = textField.getText();
            if (actualValue.equals(valueFromUser)) {
                fillKnown();
                setInfo(true);
                updateStats(actual.getKey(), true);
                iterator.remove();
            } else {
                setInfo(false);
                updateStats(actual.getKey(), false);
            }
            if (!iterator.hasNext()) {
                iterator = copy.entrySet().iterator();
            }
            if (copy.size() <= orgMap.size() / 2) {
                CourseInfo.showInfo("Ukończyłeś praktykę kursu podstawowego!\n Aby móc korzystać z większej ilości słówek wykup kurs rozszerzony!");
                flag.setText("DONE");
            }
        }
    }
}
