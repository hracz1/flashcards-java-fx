package pl.hub.lang.practices;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import pl.hub.lang.statistics.Statistics;
import pl.hub.lang.statistics.GenStatisticsService;
import pl.hub.model.User;
import pl.hub.utils.CourseInfo;
import pl.hub.utils.DataTransferDTO;
import pl.hub.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractPractise implements Practise {
    private static final String INIT = "Podaj znaczenie słowa: ";

    protected final File file;
    private final Label label;
    private final Label validLabel;
    protected Button validate;
    protected TextField textField;
    protected ListView<String> unknown;
    protected ListView<String> known;
    protected Label flag;
    private ObservableList<String> knownWords;
    private ObservableList<String> unknownWords;
    private final Label stats;
    private final Label grade;

    private final User user;
    private int index;
    private Statistics statistics;
    private GenStatisticsService genStatisticsService;

    private static final String INIT_STATS = "% poprawnych słówek:";
    private static final String INIT_GRADE = "Szacowana ocena na egzaminie: ";

    public AbstractPractise(DataTransferDTO dataTransferDTO) {
        this.validate = (Button) dataTransferDTO.get("btn");
        this.textField = (TextField) dataTransferDTO.get("txF");
        this.file = (File) dataTransferDTO.get("plik");
        this.label = (Label) dataTransferDTO.get("label");
        this.unknown = (ListView<String>) dataTransferDTO.get("unknown");
        this.known = (ListView<String>) dataTransferDTO.get("known");
        this.validLabel = (Label) dataTransferDTO.get("validLabel");
        this.stats = (Label) dataTransferDTO.get("stats");
        this.grade = (Label) dataTransferDTO.get("grade");
        this.flag = (Label) dataTransferDTO.get("flag");
        this.user = (User) dataTransferDTO.get("user");

        init();
        customListeners();

    }

    private void init() {
        fillUnknown();
        knownWords = FXCollections.observableArrayList();
        known.setItems(knownWords);
        label.setText(INIT + unknownWords.get(index));
        textField.requestFocus();
        statistics = new Statistics(file);
        genStatisticsService = new GenStatisticsService(statistics, user);

    }

    private void customListeners() {
        validate.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            commonListener();
            textField.requestFocus();
        });
        textField.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode().getName().equals("Enter")) {
                commonListener();
                textField.setText("");
            }
        });
    }

    private void commonListener() {
        loopOver();
        if (!unknownWords.isEmpty()) {
            index = (index + 1) % unknownWords.size();
        }
        label.setText(INIT + (!unknownWords.isEmpty() ? unknownWords.get(index) : ""));
        checkStates();
        stats.setText(INIT_STATS + " " + String.format("%.2f", statistics.getAtAll()));
        grade.setText(INIT_GRADE + statistics.estimateGrade());
    }

    private void checkStates() {
        if (unknown.getItems().isEmpty()) {
            flag.setText("DONE");
        }
    }

    private void fillUnknown() {
        Map<String, String> map = FileUtils.getFlashesAsMap(file);
        if (map == null) {
            CourseInfo.showError("Mapa fiszek nie może został utworzona!");
            return;
        }
        List<String> list = new ArrayList<>(map.keySet());
        unknownWords = FXCollections.observableList(list);
        unknown.setItems(unknownWords);

    }

    final void fillKnown() {
        String item = unknownWords.get(index);
        unknownWords.remove(index);
        index--;
        unknown.setItems(unknownWords);

        knownWords.add(item);
        known.setItems(knownWords);

    }

    public void setInfo(boolean cond) {
        setInfo(cond, "Poprawnie!", "Niepoprawnie!");
    }

    public void setInfo(boolean cond, String valid, String invalid) {
        if (!validLabel.isVisible()) {
            validLabel.setVisible(true);
        }
        if (cond) {
            validLabel.setText(valid);
        } else {
            validLabel.setText(invalid);
        }
    }

    void updateStats(String key, boolean valid) {
        statistics.updateStats(key, valid);
    }

    public Statistics getStatistics() {
        return statistics;
    }

    abstract protected void loopOver();

    @Override
    public Statistics returnStatsObjectAndSaveData() {
        genStatisticsService.saveStats();
        return statistics;
    }
}
