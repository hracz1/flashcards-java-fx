package pl.hub.lang.practices;

import java.util.Objects;

public class Word {
    private String pol;
    private String ang;
    private int good;
    private int bad;

    public Word(String pol, String ang) {
        this.pol = pol;
        this.ang = ang;
    }

    public double submit() {
        return 100.0 * good / (good + bad);
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getAng() {
        return ang;
    }

    public void setAng(String ang) {
        this.ang = ang;
    }

    public int getGood() {
        return good;
    }

    public void setGood(int good) {
        this.good = good;
    }

    public int getBad() {
        return bad;
    }

    public void setBad(int bad) {
        this.bad = bad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(pol, word.pol) &&
                Objects.equals(ang, word.ang);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pol, ang, good, bad);
    }
}
