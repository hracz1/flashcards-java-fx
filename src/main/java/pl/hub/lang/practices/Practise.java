package pl.hub.lang.practices;

import pl.hub.lang.statistics.Statistics;

public interface Practise {
    void practise();
    Statistics returnStatsObjectAndSaveData();
}
