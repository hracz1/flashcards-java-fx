package pl.hub.consts;

public class PatternCert {
    public static final String winPattern = """
              ------------------CERTYFIKAT------------------  
             Z przyjemnością pragnę poinformować że Pan/Pani 
                                  :name                 
               osiągnął/ła :results na egzaminie znajomości  
                            języka angielskiego              
                       tym samym zdobywając ocenę :grade     
            """;

    public static final String losePattern = """
            -------------------PORAŻKA-------------------- 
                          Niestety Pan/Pani                
                                :name                  
             osiągnął/ła :results na egzaminie znajomości  
                          języka angielskiego              
              tym samym nie zaliczając testu ostatecznego  
            """;
}
