
### What is this repository for? ###

* Learning english using flashcards
* Java 15 + JavaFX15 + MySql 
* Used IntelliJ Ultimate 2020.2.2, MySql Workbench 8.1 for development

### Basic config ###

Clone project and setup connection with db as specified in DbHelper.java. Change properties of mail in MailConfig.java class. 

### Database configuration ###

CREATE TABLE `flashes` (
   `id` int NOT NULL AUTO_INCREMENT,
   `pol` varchar(45) NOT NULL,
   `ang` varchar(45) NOT NULL,
   `usersId` int DEFAULT NULL,
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
 
CREATE TABLE `messages` (
   `id` int NOT NULL AUTO_INCREMENT,
   `text` varchar(250) DEFAULT NULL,
   `userId` int DEFAULT NULL,
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
 
CREATE TABLE `salts` (
   `idSalts` int NOT NULL AUTO_INCREMENT,
   `value` varchar(100) DEFAULT NULL,
   `bytes` varchar(100) DEFAULT NULL,
   `length` int DEFAULT '16',
   PRIMARY KEY (`idSalts`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `statistics` (
   `id` int NOT NULL AUTO_INCREMENT,
   `good` int DEFAULT NULL,
   `all_fc` int DEFAULT NULL,
   `exam_attempts` int DEFAULT NULL,
   `positive_exam_attempts` int DEFAULT NULL,
   `idUser` int DEFAULT NULL,
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `users` (
   `idUsers` int NOT NULL AUTO_INCREMENT,
   `login` varchar(45) NOT NULL,
   `password` varchar(45) NOT NULL,
   `role` varchar(7) DEFAULT 'COMMON',
   `status` varchar(1) DEFAULT 'T',
   PRIMARY KEY (`idUsers`),
   UNIQUE KEY `login_UNIQUE` (`login`),
   UNIQUE KEY `idUsers_UNIQUE` (`idUsers`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

### Run instructions ###

Use javafx-run plugin to start application and mvn javafx:run@debug maven command to debug.